<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: SxBomRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: SxBomRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("Company.class.php");
require_once("UseCase.class.php");
require_once("BomModel.class.php");
require_once("BomItemModel.class.php");

/**
 * Description of SxBomRestHandler
 *
 * @author IZIordanov
 */
class SxBomRestHandler extends SimpleRest {

    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option() {
        $mn = "SxBomRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new SxBomRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping() {
        $mn = "SxBomRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="BomModel">
    
    public function BomModelSave($value) {
        $mn = "SxBomRestHandler::BomModelSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BomModel::Save($value);
            $response = new Response("success", "BOM data saved.");
            $response->addData("bom", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BomModelGet($value) {
        $mn = "SxBomRestHandler::BomModelGet()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj =  BomModel::LoadById($value);
            $response = new Response("success", "Bom get.");
            $response->addData("bom", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BomModelDelete($bom_id) {
        $mn = "SxBomRestHandler::BomModelDelete(".$bom_id.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = BomModel::Delete($bom_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Bom deleted.");
            $response->addData("bomId", $id);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="BomItemModel">
    
    public function BomItemModelSave($value) {
        $mn = "SxBomRestHandler::BomItemModelSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = BomItemModel::Save($value);
            $response = new Response("success", "BOM item data saved.");
            $response->addData("item", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BomItemModelImport($value) {
        $mn = "SxBomRestHandler::BomItemModelImport()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $affected_row_ids = BomItemModel::Import($value);
            $response = new Response("success", "BOM item data saved.");
            $response->addData("bomItemIds", $affected_row_ids);
            $response->addData("bomId", $value->bomId);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BomItemModelGet($value) {
        $mn = "SxBomRestHandler::BomItemModelGet()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj =  BomItemModel::LoadById($value);
            $response = new Response("success", "Bom get.");
            $response->addData("item", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function BomItemModelDelete($bomitem_id) {
        $mn = "SxBomRestHandler::BomModelDelete(".$bomitem_id.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = BomItemModel::Delete($bomitem_id, $conn, $mn, $logModel);
            
            $response = new Response("success", "Bom item deleted.");
            $response->addData("bomItemId", $id);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}
