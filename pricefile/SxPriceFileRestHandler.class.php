<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: SxPriceFileRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: SxPriceFileRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("Company.class.php");

/**
 * Description of SxPriceFileRestHandler
 *
 * @author IZIordanov
 */
class SxPriceFileRestHandler extends SimpleRest {

    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option() {
        $mn = "SxPriceFileRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new SxPriceFileRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping() {
        $mn = "SxPriceFileRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Price File">
    
    public function PriceFileUpload($company_id, $auth_data) {
        $mn = "SxPriceFileRestHandler::PriceFileUpload()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $com = new Company();
            $com->LoadById($company_id);
            SxLogger::log($mn, " getTypeID = " . $com->getTypeID());  
            if($com->getTypeID() == 2){
                $file_name = $_FILES['price_file']['name'];
                $file_tmp = $_FILES['price_file']['tmp_name'];
                // the size in bytes of the uploaded file
                $file_size = $_FILES['price_file']['size'];
                $file_type = $_FILES['price_file']['type'];
                //$file_ext=strtolower(end(explode('.',$_FILES['price_file']['name'])));
                SxLogger::log($mn, " file_type = " . $file_type);  
                SxLogger::log($mn, " file_name = " . $file_name);  
                SxLogger::log($mn, " file_size = " . $file_size); 
                $extensions= array("txt","csv","tab");
                /*
                if(in_array($file_ext,$extensions)=== false){
                    $response = new Response("error", 'extension not allowed, please choose a txt, csv or tab file');
                    $response->statusCode = 412;
                } else{*/
                    /*
                    if($file_size > 2097152) {
                        $errors[]='File size must be excately 2 MB';
                     }
                     **/
                    $docRoot =  "/home/iordanov/common";
                    $comPath = "/sx/price_file/c".$com->getId();
                    SxLogger::log($mn, " file_name = " . $comPath);  
                    if (!is_dir($docRoot . $comPath)) {
                        mkdir($docRoot . $comPath, 0777, true);
                    }

                    $fname = $docRoot . $comPath."/".$file_name;
                    if (file_exists($fname)) {
                        !unlink($fname);
                    }
                    if (move_uploaded_file($file_tmp, $fname)) {
                        SxLogger::log($mn, "File is valid, and was successfully uploaded as " . $fname);
                        $sql = "INSERT INTO iordanov_sx.sx_price_file
                           (price_file_name, price_file_size, company_id,
                           full_path, price_file_type, price_file_status,
                           note) VALUES(?, ?, ?, ?, ?, ?, ? )";
                        $bound_params_r = ["siissis", 
                           $file_name, $file_size, $com->getId(),
                           $fname, $file_type, 1, 'File uploaded from user_id: '.$auth_data->user_id];
                        $pfid = $conn->preparedInsert($sql, $bound_params_r, $logModel);
                        $response->addData("price_file_id", $pfid);
                    } else {
                        $error = error_get_last();
                        SxLogger::log($mn, " Possible file upload attack. Upload failed for " . $error[0]);
                        $response = new Response("error", 'File upload failed. ');
                        $response->addData("error", $error);
                        $response->addData("file_errors", $_FILES["file"]["error"]);
                        $response->statusCode = 412;
                    }
                    
                    
                    
                    
                
                
            } else {
                $response = new Response("error", 'File upload not allowed for company type.');
                $response->statusCode = 412;
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
}
