<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: MfrItemRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: MfrItemRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("Company.class.php");
require_once("UseCase.class.php");

/**
 * Description of MfrItemRestHandler
 *
 * @author IZIordanov
 */
class MfrItemRestHandler extends SimpleRest {

    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option() {
        $mn = "MfrItemRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new MfrItemRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping() {
        $mn = "MfrItemRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Mfr Items Import">

    public function MfrItemsImport($dataJson) {
        $mn = "MfrItemRestHandler::ItemsImport()";
        SxLogger::logBegin($mn);
        $response = new Response();
        $affected_row_ids = array();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);

            if (isset($dataJson->mfr_items_import)) {
                $file_import = $dataJson->file_info;

                $items = $dataJson->mfr_items_import;
                SxLogger::log($mn, "Import mfr items -> " . sizeof($items));
                if (sizeof($items) > 0) {
                    if (isset($file_import)) {
                        if (isset($file_import->file_import_id)) {
                            $affected_row_ids = $this->MfrItemsImportCreate($file_import->file_import_id, $items, $conn, $mn, $logModel);
                        }
                    }
                }
            }

            // SxLogger::log($mn, "sizeof affected_row_ids =" . sizeof($affected_row_ids));
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

            $response->addData("affected_row_ids", $affected_row_ids);
            $response->addData("file_info", $file_import);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    /*
      $file_import = (object) [
      'file_name' => 'Default File Import',
      'file_size' => 0,
      'file_rows' =>sizeof($items),
      'company_id' => ((!isset($items[0]->mfr_company_id)) ? null : $items[0]->mfr_company_id),
      'user_id' => ((!isset($items[0]->user_id)) ? null : $items[0]->user_id),
      ];



      if(isset($file_import->file_import_id)){
      MfrItemRestHandler::MfrItemImportFileUpdate($file_import, $conn, $mn, $logModel);
      $file_import = MfrItemRestHandler::MfrItemImportFileJson($file_import->file_import_id,$conn, $mn, $logModel );
      }
      else{
      $id = MfrItemRestHandler::MfrItemImportFileCreate($file_import, $conn, $mn, $logModel);
      $file_import = MfrItemRestHandler::MfrItemImportFileJson($id,$conn, $mn, $logModel );
      }
     */

    function MfrItemsImportCreate($file_import_id, $items, $conn, $mn, $logModel) {
        $rowsInserted = 0;
        $new_ids = array();
        $strSQL = "INSERT INTO iordanov_mfr.mfr_item_import
        ( item_status_id, mfr_file_import_id, upc, ean, gtin,
        mfr_item_pik, mfr_company_id, mfr_catalog_code, mfr_item_name, mfr_ucc, 
        mfr_country_of_origin_code, country_of_origin_id, industry_id, mfr_item_description, invoice_description, 
        image_url, thumb_url, web_thumb_ur, brand_name, item_product_id, 
        lead_free, buy_amerika_complain, edms_pik, item_id, mfr_item_id, user_id)
        VALUES( ?, ?, ?, ?, ?, 
                ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?, 
                ?, ?, ?, ?, ?,
                ?, ?, ?, ?, ?, ?)";

        foreach ($items as $dataJson) {
            $bound_params_r = ["iisssiissssiissssssiiiiiii",
                ((!isset($dataJson->item_status_id)) ? null : $dataJson->item_status_id),
                $file_import_id,
                ((!isset($dataJson->upc)) ? null : $dataJson->upc),
                ((!isset($dataJson->ean)) ? null : $dataJson->ean),
                ((!isset($dataJson->gtin)) ? null : $dataJson->gtin),
                ((!isset($dataJson->mfr_item_pik)) ? null : $dataJson->mfr_item_pik),
                ((!isset($dataJson->mfr_company_id)) ? null : $dataJson->mfr_company_id),
                ((!isset($dataJson->mfr_catalog_code)) ? null : $dataJson->mfr_catalog_code),
                ((!isset($dataJson->mfr_item_name)) ? null : $dataJson->mfr_item_name),
                ((!isset($dataJson->mfr_ucc)) ? null : $dataJson->mfr_ucc),
                ((!isset($dataJson->mfr_country_of_origin_code)) ? null : $dataJson->mfr_country_of_origin_code),
                ((!isset($dataJson->country_of_origin_id)) ? null : $dataJson->country_of_origin_id),
                ((!isset($dataJson->industry_id)) ? 2 : $dataJson->industry_id),
                ((!isset($dataJson->mfr_item_description)) ? null : $dataJson->mfr_item_description),
                ((!isset($dataJson->invoice_description)) ? null : $dataJson->invoice_description),
                ((!isset($dataJson->image_url)) ? null : $dataJson->image_url),
                ((!isset($dataJson->thumb_url)) ? null : $dataJson->thumb_url),
                ((!isset($dataJson->web_thumb_ur)) ? null : $dataJson->web_thumb_ur),
                ((!isset($dataJson->brand_name)) ? null : $dataJson->brand_name),
                ((!isset($dataJson->item_product_id)) ? null : $dataJson->item_product_id),
                ((!isset($dataJson->lead_free)) ? null : $dataJson->lead_free),
                ((!isset($dataJson->buy_amerika_complain)) ? null : $dataJson->buy_amerika_complain),
                ((!isset($dataJson->edms_pik)) ? null : $dataJson->edms_pik),
                ((!isset($dataJson->item_id)) ? null : $dataJson->item_id),
                ((!isset($dataJson->mfr_item_id)) ? null : $dataJson->mfr_item_id),
                ((!isset($dataJson->user_id)) ? null : $dataJson->user_id),
            ];
            $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
            SxLogger::log($mn, " value of id=" . $id);
            $new_ids[$rowsInserted] = $id;
            $rowsInserted++;
        }
        SxLogger::log($mn, " size of new_ids=" . sizeof($new_ids));

        return $new_ids;
    }

    function MfrItemsImportClearByFileId($file_import_id, $conn, $mn, $logModel) {
        $affectedRows = 0;
       
        $sql = "DELETE  FROM iordanov_mfr.mfr_item_import
            where mfr_file_import_id=?";

        $bound_params_r = ["i",
            (!isset($file_import_id) ? null : $file_import_id),
        ];
        $affectedRows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);

        return $affectedRows;
    }
    
    public function MfrItemsImportDeleteByFileId($dataJson) {
        $mn = "MfrItemRestHandler::MfrItemsImportDeleteByFileId()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $affectedRows = -1;
            if (isset($dataJson->file_import_id)) {
                SxLogger::log($mn, "Cliear items for file_import_id =" . $dataJson->file_import_id);
                $affectedRows = MfrItemRestHandler::MfrItemsImportClearByFileId($dataJson->file_import_id, $conn, $mn, $logModel);
                $file_import = MfrItemRestHandler::MfrItemImportFileJson($dataJson->file_import_id, $conn, $mn, $logModel);
            } 

            SxLogger::log($mn, " deleted rows =" . $affectedRows);
            
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("file_import", $file_import);
            $response->addData("affected_rows", $affectedRows);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="MfrItemImportFile Table">

    public function MfrItemImportFileTable($params) {
        $mn = "MfrItemRestHandler::MfrItemImportFileTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT p.file_import_id, p.file_name,
                    p.file_size, p.file_rows, p.file_data_fields, 
                    p.company_id, p.user_id, p.processing_status_id, 
                    p.stat_rows_total, p.stat_rows_processed_ok,  
                    p.adate, p.udate,
                    c.company_name, u.user_name
                    FROM iordanov_mfr.mfr_item_import_file p
                    join iordanov_sm.sm_company c on c.company_id = p.company_id
                    join iordanov_sx.sx_user u on u.user_id = p.user_id ";



            if (isset($params->company_id) && strlen($params->company_id) > 0) {
                $sqlWhere = " WHERE p.company_id = " . $params->company_id . " ";
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere)) {
                    $sqlWhere .= " AND (p.file_name like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or u.user_name like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere = " WHERE (p.file_name like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or u.user_name like '%" . $params->qry_filter . "%' )";
                }
            }

            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by p." . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= "order by p.adate desc ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("file_list", $ret_json_data);

            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_mfr.mfr_item_import_file p
                    join iordanov_sm.sm_company c on c.company_id = p.company_id
                    join iordanov_sx.sx_user u on u.user_id = p.user_id " . $sqlWhere . " and 1=?";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="mfr_item_import_file FileImportModel">

    public function MfrItemImportFileSave($dataJson) {
        $mn = "MfrItemRestHandler::MfrItemImportFileSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if (isset($dataJson->file_import_id)) {
                SxLogger::log($mn, "Update FileImportModel file_import_id =" . $dataJson->file_import_id);
                $id = $dataJson->file_import_id;
                $id = MfrItemRestHandler::MfrItemImportFileUpdate($dataJson, $conn, $mn, $logModel);
            } else {
                SxLogger::log($mn, "Create File Import");
                $id = MfrItemRestHandler::MfrItemImportFileCreate($dataJson, $conn, $mn, $logModel);
            }

            SxLogger::log($mn, "file_import_id =" . $id);
            $ret_json_data = MfrItemRestHandler::MfrItemImportFileJson($id, $conn, $mn, $logModel);
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("file_import", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    function MfrItemImportFileCreate($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_mfr.mfr_item_import_file
            (file_name, file_size, file_rows,
            file_data_fields, file_header, company_id, user_id)
            VALUES(?, ?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["siiisii",
            ((!isset($dataJson->file_name)) ? null : $dataJson->file_name),
            ((!isset($dataJson->file_size)) ? null : $dataJson->file_size),
            ((!isset($dataJson->file_rows)) ? null : $dataJson->file_rows),
            ((!isset($dataJson->file_data_fields)) ? null : $dataJson->file_data_fields),
            ((!isset($dataJson->file_header)) ? null : $dataJson->file_header),
            ((!isset($dataJson->company_id)) ? null : $dataJson->company_id),
            ((!isset($dataJson->user_id)) ? null : $dataJson->user_id)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);

        return $id;
    }

    function MfrItemImportFileUpdate($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_mfr.mfr_item_import_file
            SET
            file_name=?, file_size=?, file_rows=?,
            file_data_fields=?, file_header=?, company_id=?, user_id=?
            WHERE file_import_id = ? ";

        $bound_params_r = ["siiisiii",
            ((!isset($dataJson->file_name)) ? null : $dataJson->file_name),
            ((!isset($dataJson->file_size)) ? null : $dataJson->file_size),
            ((!isset($dataJson->file_rows)) ? null : $dataJson->file_rows),
            ((!isset($dataJson->file_data_fields)) ? null : $dataJson->file_data_fields),
            ((!isset($dataJson->file_header)) ? null : $dataJson->file_header),
            ((!isset($dataJson->company_id)) ? null : $dataJson->company_id),
            ((!isset($dataJson->user_id)) ? null : $dataJson->user_id),
            ($dataJson->file_import_id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->project_id;
    }

    function MfrItemImportFileJson($file_import_id, $conn, $mn, $logModel) {

        $sql = "SELECT p.file_import_id, p.file_name,
                    p.file_size, p.file_rows, p.file_data_fields, p.file_header,
                    p.company_id, p.user_id, p.processing_status_id, 
                    p.stat_rows_total, p.stat_rows_processed_ok,  
                    p.adate, p.udate,
                    c.company_name, u.user_name
                    FROM iordanov_mfr.mfr_item_import_file p
                    join iordanov_sm.sm_company c on c.company_id = p.company_id
                    join iordanov_sx.sx_user u on u.user_id = p.user_id
                    WHERE p.file_import_id = ?";

        $bound_params_r = ["i", $file_import_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    // </editor-fold>
}
