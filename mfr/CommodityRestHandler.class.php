<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: CommodityRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: CommodityRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("Company.class.php");
require_once("UseCase.class.php");

/**
 * Description of CommodityRestHandler
 *
 * @author IZIordanov
 */
class CommodityRestHandler extends SimpleRest {

    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option() {
        $mn = "CommodityRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new CommodityRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping() {
        $mn = "CommodityRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Commodity">
    
    public function CommoditySave($dataJson) {
        $mn = "CommodityRestHandler::CommoditySave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $project_id = null;
            if(isset($dataJson->project_id)){
               SxLogger::log($mn, "Update project project_id =" . $dataJson->project_id);
               $project_id = $dataJson->project_id;
               $project_id = $this->ProjectUpdate($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create project");
                $project_id = $this->ProjectCreate($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, "project_id =" . $project_id);
            $ret_json_data = $this->ProjectJson($project_id, $conn, $mn, $logModel);
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("project", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    function ProjectCreate($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_project
            (project_name, pstatus_id, company_id,
            project_account_id, estimator_id, manager_id,
            notes, start_date, end_date)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["siisiisss",
            ((!isset($dataJson->project_name)) ? null : $dataJson->project_name),
            ((!isset($dataJson->pstatus_id)) ? null : $dataJson->pstatus_id),
            ((!isset($dataJson->company_id)) ? null : $dataJson->company_id),
            ((!isset($dataJson->project_account_id)) ? null : $dataJson->project_account_id),
            ((!isset($dataJson->estimator_id)) ? null : $dataJson->estimator_id),
            ((!isset($dataJson->manager_id)) ? null : $dataJson->manager_id),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            (DateTimeDbStr($dataJson->start_date) ),
            (DateTimeDbStr($dataJson->end_date)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    function ProjectUpdate($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_project
            SET
            project_name=?, pstatus_id=?, company_id=?,
            project_account_id=?, estimator_id=?, manager_id=?,
            notes=?, start_date=?, end_date=?
            WHERE project_id = ? " ;

        $bound_params_r = ["siisiisssi",
            ((!isset($dataJson->project_name)) ? null : $dataJson->project_name),
            ((!isset($dataJson->pstatus_id)) ? null : $dataJson->pstatus_id),
            ((!isset($dataJson->company_id)) ? null : $dataJson->company_id),
            ((!isset($dataJson->project_account_id)) ? null : $dataJson->project_account_id),
            ((!isset($dataJson->estimator_id)) ? null : $dataJson->estimator_id),
            ((!isset($dataJson->manager_id)) ? null : $dataJson->manager_id),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            (DateTimeDbStr($dataJson->start_date) ),
            (DateTimeDbStr($dataJson->end_date)),
            ($dataJson->project_id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->project_id;
    }
    
    function ProjectJson($project_id, $conn, $mn, $logModel){
        
        $sql = "SELECT p.project_id, p.project_name,
                    p.pstatus_id, p.company_id, p.project_account_id, 
                    p.estimator_id, e.user_name as estimator_name, e.e_mail  as estimator_email,
                    p.manager_id, m.user_name as manager_name, m.e_mail  as manager_email,
                    p.notes, p.start_date, p.end_date, p.adate, p.udate
                    FROM iordanov_psm.psm_project p
                    left join iordanov_sx.sx_user e on e.user_id = p.estimator_id 
                    left join iordanov_sx.sx_user m on m.user_id = p.manager_id
                    WHERE p.project_id = ? " ;

        $bound_params_r = ["i",$project_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Project Table">
    
    public function CommodityList($params) {
        $mn = "CommodityRestHandler::CommodityList()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.commodity_id, c.parent_commodity_id, 
            c.commodity_type_id, c.commodity_code, c.commodity_name,
            c.commodity_description, c.commodity_image_url, c.display_idx,
            if(c4.industry_id=4,1,0) electrical, 
            if(c1.industry_id=1,1,0) plumbing, 
            if(c3.industry_id=3,1,0) hvacr,
            if(c2.industry_id=2,1,0) industrial
            FROM iordanov_mfr.cfg_commodity c
            LEFT JOIN iordanov_mfr.cfg_commodity_industry c4 on c4.commodity_id = c.commodity_id and c4.industry_id=4
            LEFT JOIN iordanov_mfr.cfg_commodity_industry c1 on c1.commodity_id = c.commodity_id and c1.industry_id=1
            LEFT JOIN iordanov_mfr.cfg_commodity_industry c2 on c2.commodity_id = c.commodity_id and c2.industry_id=2
            LEFT JOIN iordanov_mfr.cfg_commodity_industry c3 on c3.commodity_id = c.commodity_id and c3.industry_id=3 
            where c.commodity_type_id = ? ";
            $sqlWhere = "";
            if(isset($params->parent_commodity_id) && strlen($params->parent_commodity_id)>1){
                $sqlWhere .= " and c.parent_commodity_id = ?";
                $bound_params_r = ["ii", $params->commodity_type_id, $params->parent_commodity_id];
            }
            else{
                $sqlWhere .= " and c.parent_commodity_id is null";
                $bound_params_r = ["i", $params->commodity_type_id];  
            }
            
            if(isset($params->qry_orderCol)){
                $sqlOrder = " order by c.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder = " order by c.display_idx, c.commodity_code ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            
            SxLogger::log($mn, " sql= " . $sql . " ");
            
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("commodity_list", $ret_json_data);
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
   
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="UseCase">
    
    public function UseCaseSave($dataJson) {
        $mn = "CommodityRestHandler::UseCaseSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->usecase_id)){
               SxLogger::log($mn, "Update usecase usecase_id =" . $dataJson->usecase_id);
               $id = $dataJson->usecase_id;
               $id = UseCase::UseCaseUpdate($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create usecase");
                $id = UseCase::UseCaseCreate($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, "usecase_id =" . $id);
            $ret_json_data = UseCase::UseCaseJson($id, $conn, $mn, $logModel);
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("usecase", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function UseCaseTable($params) {
        $mn = "CommodityRestHandler::UseCaseTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT uc.usecase_id, uc.project_id, p.project_name, p.project_account_id,
                    uc.usecase_name, uc.usecase_order, uc.pstatus_id, uc.usecase_notes,
                    uc.usecase_img_url, uc.adate, uc.user_id, u.user_name, u.e_mail, uc.udate
                    FROM iordanov_psm.psm_usecase uc
                    left join iordanov_psm.psm_project p on p.project_id = uc.project_id
                    left join iordanov_sx.sx_user u on u.user_id = uc.user_id 
                    ";
            
            
            
            if(isset($params->project_id) && strlen($params->project_id)>0){
                $sqlWhere = " WHERE uc.project_id = ".$params->project_id." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (uc.usecase_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or uc.usecase_notes like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (uc.usecase_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or uc.usecase_notes like '%".$params->qry_filter."%' )";
                }
               
            }
            
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by usecase_order, usecase_name ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("usecase_table", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_psm.psm_usecase  uc ".$sqlWhere." and 1=?"  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}
