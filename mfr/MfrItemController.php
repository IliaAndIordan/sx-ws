<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : MfrItemController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
        $delim . '/home/iordanov/common/lib/sx' .
        $delim . '/home/iordanov/common/lib/sx/models' .
        $delim . '/home/iordanov/common/lib/sx/models/psm' .
        $delim . '/home/iordanov/iordanov.info/sx/sx-ws' .
        $delim . '/home/iordanov/iordanov.info/sx/sx-ws/company' .
        $delim . '/home/iordanov/iordanov.info/sx/sx-ws/project' .
        $delim . '/home/iordanov/common/lib/log4php' .
        $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();
//echo "MfrItemController.php";
$mn = "MfrItemController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("../SxConnection.php");
require_once("../SxLogger.php");
require_once("Functions.php");
require_once("../SxRestHandler.class.php");
require_once("MfrItemRestHandler.class.php");
require_once("Company.class.php");

SxLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"]))
    $view = $_REQUEST["view"];

SxLogger::log($mn, " -> view: " . $view);
SxLogger::log($mn, " -> REQUEST_METHOD: " . $_SERVER['REQUEST_METHOD']);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new MfrItemRestHandler();
    $restHendler->Option();
    SxLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];

    $payloadJson;
    $payloadAuth;

    $authRes = JwtAuth::Autenticate();

    if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new MfrItemRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    } else {
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new MfrItemRestHandler();
        $rh->EncodeResponce($response);
        return;
    }

    try {
        $payload = file_get_contents('php://input');
        if (isset($payload)) {
            $payloadJson = json_decode($payload);
            SxLogger::log($mn, "payloadJson=" . $payload);
        }
    } catch (Exception $ex) {
        AmsLogger::logError($mn, $ex);
    }
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {


        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new MfrItemRestHandler();
            $restHendler->Ping($id);
            SxLogger::log($mn, "ping executed");
            break;

        // <editor-fold defaultstate="collapsed" desc="Mfr Items Import">

        case "mfr_items_import":
            // to handle REST Url /pcpd/
            $rh = new MfrItemRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)) {
                $payload_json = json_decode($payload);
                // SxLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)) {
                    $rh->MfrItemsImport($payload_json);
                } else {
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            } else {
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        case "mfr_items_import_delete":
            // to handle REST Url /pcpd/
            $rh = new MfrItemRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $payload_json = json_decode($payload);
                // SxLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $rh->MfrItemsImportDeleteByFileId($payload_json);

                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
                
            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        case "mfr_item_import_file_save":
            // to handle REST Url /pcpd/
            $rh = new MfrItemRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)) {
                $dataJson = json_decode($payload);
                $data = $dataJson->mfr_item_import_file;
                if (isset($data)) {
                    //SxLogger::log($mn, "[company] company_name: " . $company->company_name . " ");
                    $rh->MfrItemImportFileSave($data);
                } else {
                    $response = new Response("error", 'File data not provided.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            } else {
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;


        case "mfr_item_import_file_table":
            $rh = new MfrItemRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)) {
                $dataJson = json_decode($payload);

                if (isset($dataJson->qry_limit)) {
                    //SxLogger::log($mn, "[company] company_name: " . $company->company_name . " ");
                    $rh->MfrItemImportFileTable($dataJson);
                } else {
                    $response = new Response("error", 'No parameters provided.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            } else {
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;

        // </editor-fold>

        default:
            SxLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


SxLogger::logEnd($mn);

