<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("../SxConnection.php");
require_once("../SxLogger.php");
require_once("Apache/Solr/Service.php");

/**
 * Description of SolrClientSxCommit
 *
 * @author IZIordanov
 */
class SolrClientSxCommit {

    // <editor-fold defaultstate="collapsed" desc="SolrClient constructors">

    private $httpVersion = "HTTP/1.1";
    private $config_solr_core = array(
        'endpoint' => array(
            'localhost' => array(
                'host' => DB_HOST,
                'port' => SOLR_PORT,
                'path' => '/solr/sx-commodity'
            )
        )
    );
    private $solr_core = null;

    public function __construct() {
        $mn = "SolrClientSxCommit:construct";
        SxLogger::logBegin($mn);
        // create a new solr service instance - host, port, and webapp
        // path (all defaults in this example)
        $this->solr_core = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/sx-commodity/');

        if (!$this->solr_core->ping()) {
            SxLogger::log($mn, "WARNING: Solr solr_core_x-commodity service not responding!");
            exit;
        }
        SxLogger::logEnd($mn);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="core Commodity">

    public function CommodityAutocomplete($strCriteria) {
        $mn = "SolrClientSxCommit::CommodityAutocomplete(" . $strCriteria . ")";
        SxLogger::logBegin($mn);
        $solr = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/sx-commodity/');
        $results = false;
        if (!isset($this->solr_core) || !$this->solr_core->ping()) {
            $this->solr_core = new Apache_Solr_Service(DB_HOST, SOLR_PORT, '/solr/sx-commodity/');
        }

        if (!$this->solr_core->ping()) {
            SxLogger::log($mn, "WARNING: Solr solr_core_x-commodity service not responding!");
            return $results;
        }

        if (!isset($strCriteria) && $strCriteria != "")
            return $results;
        
        $strCriteria = validate_search_string($strCriteria);
        $query = "text:" . $strCriteria;
        if (strlen($query) > 1) {
            // if magic quotes is enabled then stripslashes will be needed
            if (get_magic_quotes_gpc() == 1) {
                $query = stripslashes($query);
            }
            $offset = 0;
            $limit = 10;

            $method = Apache_Solr_Service::METHOD_GET;
            //&hl=true&hl.fl=aname&hl.snippets=3&hl.mergeContigious=true
            $params = array(
                "hl" => "true",
                "hl.fl" => "commodity_code,commodity_name",
                "hl.method" => "unified",
                "hl.mergeContiguous" => "true",
                "hl.tag.pre" => "<strong>",
                "hl.tag.post" => "</strong>",
                "hl.encoder" => "html"
            );

            try {
                $results = $this->solr_core->search($query, $offset, $limit, $params, $method);
                //logDebug($MN, "Solr solr_core query ".serialize($query)." successful!");
                //logDebug($MN, "responseHeader: " . serialize($results->responseHeader));
                //logDebug($MN, "response: " . serialize($results->response));
            } catch (Exception $e) {
                SxLogger::logError($mn, $ex);
            }
        }
        SxLogger::logEnd($mn);
        return $results;
    }

    // </editor-fold>
}
