<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : SxCompanyController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
    $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
    $delim . '/home/iordanov/common/lib/sx' .
    $delim . '/home/iordanov/common/lib/sx/models'.
    $delim . '/home/iordanov/common/lib/sx/models/psm' .
    $delim . '/home/iordanov/iordanov.info/sx/sx-ws'.
    $delim . '/home/iordanov/iordanov.info/sx/sx-ws/company'.
    $delim . '/home/iordanov/common/lib/log4php' .
    $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();
//echo "SxCompanyController.php";
$mn = "SxCompanyController.php";
//--- Include CORS
require_once("rest_cors_header.php");
require_once("../SxConnection.php");
require_once("../SxLogger.php");
require_once("Functions.php");
require_once("../SxRestHandler.class.php");
require_once("SxCompanyRestHandler.class.php");
require_once("Company.class.php");
require_once("CompanyItemModel.class.php");

SxLogger::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];

SxLogger::log($mn, " -> view: " . $view);
SxLogger::log($mn, " -> REQUEST_METHOD: " . $_SERVER['REQUEST_METHOD']);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new SxCompanyRestHandler();
    $restHendler->Option();
    SxLogger::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    
    $payloadJson;
    $payloadAuth;
    
    $authRes = JwtAuth::Autenticate();
    
     if (isset($authRes)) {
        if ($authRes->isValud) {
            $payloadAuth = $authRes->payload;
        } else {
            $response = new Response("error", $authRes->message);
            $response->statusCode = 401;
            $rh = new SxCompanyRestHandler();
            $rh->EncodeResponce($response);
            return;
        }
    }
    else{
        $response = new Response("error", 'Autentication is required');
        $response->statusCode = 401;
        $rh = new SxCompanyRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
    
     try{
        $payload = file_get_contents('php://input');
        if(isset($payload)){
            $payloadJson = json_decode($payload);
            SxLogger::log($mn, "payloadJson=" . $payload);
        }
    } catch (Exception $ex) {
        AmsLogger::logError($mn,  $ex);
    }
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

         
         
        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new SxCompanyRestHandler();
            $restHendler->Ping($id);
            SxLogger::log($mn, "ping executed");
            break;
        
        // <editor-fold defaultstate="collapsed" desc="Country City">
        
        case "country_list":
            $restHendler = new SxCompanyRestHandler();
            $restHendler->CountryList();
            break;
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Company">
        
        case "company":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                SxLogger::log($mn, "[company] dataJson: " . $dataJson->company_id . " ");
                $rh->Company( $dataJson->company_id);
            }  else{
                    $response = new Response("error", 'Required parameters missing in request.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            break;
            
        case "company_save":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $company = $dataJson->company;
                    if (isset($company)){
                        //SxLogger::log($mn, "[company] company_name: " . $company->company_name . " ");
                        if(!isset($company->company_name)){
                            $response = new Response("error", 'Name is required in order to create a new company');
                            $response->country_id = 412;
                            $rh->EncodeResponce($response);
                        } else if(!isset($company->country_id)){
                            $response = new Response("error", 'Country is required in order to create a new company');
                            $response->statusCode = 412;
                            $rh->EncodeResponce($response);
                        } else if(!isset($company->company_type_id)){
                            $response = new Response("error", 'Compnay type value is missing.');
                            $response->statusCode = 412;
                            $rh->EncodeResponce($response);
                        } else{ 
                            //SxLogger::log($mn, "[company]: " . json_encode($company) . " ");

                            $rh->CompanySave($company);
                        }
                    } else{
                    $response = new Response("error", 'Company data not provided.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
                
            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        case "sm_product_add":
            // to handle REST Url /pcpd/
            $rh = new PsmCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $company_id = $dataJson->company_id;
                $sm_product_id = $dataJson->sm_product_id;
                   
                //SxLogger::log($mn, "[company] company_name: " . $company->company_name . " ");
                if(!isset($company_id)){
                    $response = new Response("error", 'Company id is required in order to add product');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                } else if(!isset($sm_product_id)){
                    $response = new Response("error", 'SM Product id is required parameter');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                }  else{ 
                    //SxLogger::log($mn, "[company]: " . json_encode($company) . " ");

                    $rh->CompanySmProductAdd($company_id, $sm_product_id);
                }
            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
            case "sm_product_del":
            $rh = new PsmCompanyRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $company_id = $dataJson->company_id;
                $sm_company_product_id = $dataJson->sm_company_product_id;
                   
                //SxLogger::log($mn, "[company] company_name: " . $company->company_name . " ");
                if(!isset($company_id)){
                    $response = new Response("error", 'Company id is required in order to delete SM Product');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                } else if(!isset($sm_company_product_id)){
                    $response = new Response("error", 'SM Company Product id is required parameter');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                }  else{ 
                    //SxLogger::log($mn, "[company]: " . json_encode($company) . " ");

                    $rh->CompanySmProductDelete($company_id, $sm_company_product_id);
                }
            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
            
        case "company_livery_update":
        // to handle REST Url /pcpd/
        $rh = new SxCompanyRestHandler();
        // read JSon input
        $payload = file_get_contents('php://input');

        if (isset($payload)){
            $payload_json = json_decode($payload);
            SxLogger::log($mn, " company_id: " . $payload_json->company_id . " ");
            $company_id = $payload_json->company_id;
            if (isset($company_id)){
                $rh->CompanyLiveryUpdate($company_id);

            } else{
                $response = new Response("error", 'Company data not provided.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }

        } else{
            $response = new Response("error", 'Missing values to process');
            $response->statusCode = 412;
            $rh->EncodeResponce($response);
            return;
        }
        break;
        
        case "company_options":
            $rh = new SxCompanyRestHandler();
            $rh->CompanyOpt();
        break;
    
        case "company_list":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                SxLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $rh->CompanyListTable($payload_json);

                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        case "user_list":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                SxLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $rh->UserListTable($payload_json);

                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        
        case "company_dashboard":
        // to handle REST Url /pcpd/
        $rh = new SxCompanyRestHandler();
        // read JSon input
        $payload = file_get_contents('php://input');

        if (isset($payload)){
            $payload_json = json_decode($payload);
            SxLogger::log($mn, " companyId: " . $payload_json->companyId . " ");
            if (isset($payload_json)){
                $rh->CompanyDashboardStat($payload_json->companyId);

            } else{
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }

        } else{
            $response = new Response("error", 'Missing values to process');
            $response->statusCode = 412;
            $rh->EncodeResponce($response);
            return;
        }
        break;
        
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="Company Products">
        
        case "company_products":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                SxLogger::log($mn, "[company] dataJson: " . $dataJson->company_id . " ");
                $rh->CompanyProductsList( $dataJson->company_id);
            }  else{
                    $response = new Response("error", 'Required parameters missing in request.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
            break;
           case "company_products_add":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                SxLogger::log($mn, " product_id: " . $dataJson->product_id . " ");
                
                if(isset($dataJson->company_id) && isset($dataJson->product_id)){
                     SxLogger::log($mn, "Add product to company " . $dataJson->company_id);
                     $rh->CompanyProductAdd( $dataJson->company_id, $dataJson->product_id);
                } else{
                    $response = new Response("error", 'Required parameters missing in request.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
                
            }  else{
                $response = new Response("error", 'Required parameters missing in request.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
           case "company_products_delete":
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                SxLogger::log($mn, "company_product_id: " . $dataJson->company_product_id . " ");
                
                if(isset($dataJson->company_product_id)){
                     $rh->CompanyProductDelete( $dataJson->company_id, $dataJson->company_product_id);
                } else{
                    $response = new Response("error", 'Required parameters missing in request.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }
                
            }  else{
                $response = new Response("error", 'Required parameters missing in request.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
            
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="SxUsers">
        
        case "company_users":
        // to handle REST Url /pcpd/
        $rh = new SxCompanyRestHandler();
        // read JSon input
        $payload = file_get_contents('php://input');

        if (isset($payload)){
            $payload_json = json_decode($payload);
            SxLogger::log($mn, " company_id: " . $payload_json->company_id . " ");
            $company_id = $payload_json->company_id;
            if (isset($company_id)){
                $rh->CompanySxUsers($company_id);

            } else{
                $response = new Response("error", 'Company data not provided.');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }

        } else{
            $response = new Response("error", 'Missing values to process');
            $response->statusCode = 412;
            $rh->EncodeResponce($response);
            return;
        }
        break;
        
        case "company_users_table":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                SxLogger::log($mn, " company_id: " . $payload_json->company_id . " ");
                if (isset($payload_json)){
                    $rh->CompanySxUsersTable($payload_json);

                } else{
                    $response = new Response("error", 'Company data not provided.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
            break;
        // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="CompanyItemModel">
        
        case "citem_table":
            // to handle REST Url /pcpd/
            $rh = new SxCompanyRestHandler();
            // read JSon input
            $payload = file_get_contents('php://input');

            if (isset($payload)){
                $payload_json = json_decode($payload);
                SxLogger::log($mn, " qry_limit: " . $payload_json->qry_limit . " ");
                if (isset($payload_json)){
                    $response = CompanyItemModel::GetTable($payload_json);
                    $rh->EncodeResponce($response);
                    return;
                } else{
                    $response = new Response("error", 'Missing required parameters.');
                    $response->statusCode = 412;
                    $rh->EncodeResponce($response);
                    return;
                }

            } else{
                $response = new Response("error", 'Missing values to process');
                $response->statusCode = 412;
                $rh->EncodeResponce($response);
                return;
            }
        break;
        
        case "citem_save":
            $rh = new SxCompanyRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CompanyItemModelSave($dataJson->item);
            }
            break;
        case "citem_import":
            $rh = new SxCompanyRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CompanyItemModelImport($dataJson);
            }
            break;
        case "citem_get":
            $rh = new SxCompanyRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CompanyItemModelGet($dataJson->citemId);
            }
            break;
            
        case "citem_delete":
            $rh = new SxCompanyRestHandler();
            $payload = file_get_contents('php://input');
            
            if (isset($payload)){
                $dataJson = json_decode($payload);
                $rh->CompanyItemModelDelete($dataJson->citemId);
            }
            break;   
        // </editor-fold>
            
        default:
            SxLogger::log($mn, "No heandler for view: " . $view);
            break;
    }
}


SxLogger::logEnd($mn);

