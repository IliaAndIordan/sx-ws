<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: SxCompanyRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: SxCompanyRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("Company.class.php");
require_once("CompanyItemModel.class.php");

/**
 * Description of SxCompanyRestHandler
 *
 * @author IZIordanov
 */
class SxCompanyRestHandler extends SimpleRest {

    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option() {
        $mn = "SxCompanyRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new SxCompanyRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping() {
        $mn = "SxCompanyRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Country City">
    
    public function CountryList() {
        $mn = "SxCompanyRestHandler::CountryList()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.country_id, c.country_name, c.country_iso2, 
                r.region_name, sr.subregion_name, c.region_id, c.subregion_id 
                FROM iordanov_ams_wad.cfg_rs_country c
                join iordanov_ams_wad.cfg_region r on r.region_id = c.region_id  
                join iordanov_ams_wad.cfg_subregion sr on sr.subregion_id = c.subregion_id
                where 1=?
                ORDER BY c.country_name";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("country_list", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Company">

    public function Company($id) {
        $mn = "SxCompanyRestHandler::Company(" . $id . ")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $com = new Company();
            $com->LoadById($id);
            $response->addData("company", $com);
            $sql = "SELECT c.company_id, 
                    count(distinct(u.user_id)) as usersCount,
                    count(distinct(p.company_product_id)) as productsCount,
                    c.adate, c.udate
                    FROM iordanov_sm.sm_company c
                    left join iordanov_sx.sx_user u on u.company_id = c.company_id 
                    left join iordanov_sm.sm_company_product p on p.company_id = c.company_id
                    where c.company_id = ? group by c.company_id";
            $bound_params_r = ["i", $id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("details", $ret_json_data[0]);
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }

    public function CompanySave($dataJson) {
        $mn = "SxCompanyRestHandler::CompanySave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $com = new Company();
            $com->setId($dataJson->company_id);
            $com->setName($dataJson->company_name);
            $com->setTypeID($dataJson->company_type_id);
            $com->setParentId($dataJson->parent_company_id);
            $com->setBranch($dataJson->branch_code);
            $com->setLogoUrl($dataJson->logo_url);
            $com->setWebUrl($dataJson->web_url);
            $com->setNodes($dataJson->notes);
            $com->setEanMfrCode($dataJson->ean_mfr_code);
            $com->setCountryId($dataJson->country_id);
            $com->setActorId($dataJson->actor_id);
            $com->setStatusId($dataJson->company_status_id);

            $ret_json_data = $com->save();
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("company", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyLiveryUpdate($company_id) {
        $mn = "SxCompanyRestHandler::CompanyLiveryUpdate()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $com = new Company();
            $com->LoadById($company_id);
            $com->deleteSxLiveryAndLogo();
            $com->LoadById($company_id);
            $response->addData("company", $com);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyOpt() {
        $mn = "SxCompanyRestHandler::CompanyOpt()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT company_id,  company_name, branch_code, company_type_id, logo_url, country_id
                    FROM iordanov_sm.sm_company WHERE 1=?
                    order by company_name, branch_code";
            $bound_params_r = ["i", 1];
            
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("company_opt", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyListTable($params) {
        $mn = "SxCompanyRestHandler::CompanyListTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.company_id, c.company_name,  
                    c.company_type_id, c.parent_company_id, c.branch_code, 
                    c.logo_url, c.web_url, c.notes, c.country_id, c.company_status_id,
                    count(distinct(u.user_id)) as usersCount,
                    count(distinct(p.company_product_id)) as productsCount,
                    c.adate, c.udate
                    FROM iordanov_sm.sm_company c
                    left join iordanov_sx.sx_user u on u.company_id = c.company_id 
                    left join iordanov_sm.sm_company_product p on p.company_id = c.company_id";
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                $sql .= " WHERE (c.company_name like '%".$params->qry_filter."%' ";
                $sql .= " or c.branch_code like '%".$params->qry_filter."%' )";
            }
            
            $sql .= " group by c.company_id ";
            
            if(isset($params->qry_orderCol)){
                $sql .= " order by c.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sql .= "order by c.company_name, c.branch_code";
            }
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("company_list", $ret_json_data);
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_sm.sm_company
                    where 1 = ? ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyDashboardStat($companyId) {
        $mn = "SxCompanyRestHandler::CompanyDashboardStat()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT count(i.citem_id) as itemsCount 
                    FROM iordanov_sx.sx_company_item i
                    where i.company_id = ?";
            $bound_params_r = ["i", $companyId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("companyDashboard", $ret_json_data[0]);
            
            $sql = "SELECT i.adate, count(i.citem_id) as itemsCount 
                    FROM iordanov_sx.sx_company_item i
                    where i.company_id = ?
                    group by adate";
            $bound_params_r = ["i", $companyId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("chartItemsByDate", $ret_json_data);
             
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="SxUsers">

    public function CompanySxUsers($company_id) {
        $mn = "SxCompanyRestHandler::CompanySxUsers()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id, u.user_name,
                    u.e_mail, u.user_role, u.password as u_password,
                     u.company_id, u.is_receive_emails, 
                     u.ip_address, (UNIX_TIMESTAMP(u.adate)*1000) as adate, (UNIX_TIMESTAMP(u.udate)*1000) as udate
                    FROM iordanov_sx.sx_user u
                    where company_id = ?
                    order by u.user_name, u.adate desc";
            $bound_params_r = ["i", $company_id];
            
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("users", $ret_json_data);
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_sx.sx_user u
                    where company_id = ? ";
            $bound_params_r = ["i", $company_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanySxUsersTable($params) {
        $mn = "SxCompanyRestHandler::CompanySxUsersTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id, u.user_name,
                    u.e_mail, u.user_role, u.password as u_password,
                     u.company_id, u.is_receive_emails, 
                    u.ip_address, (UNIX_TIMESTAMP(u.adate)*1000) as adate, (UNIX_TIMESTAMP(u.udate)*1000) as udate
                    FROM iordanov_sx.sx_user u
                    where company_id = ? ";
            if(isset($params->qry_filter) && strlen($params->qry_filter)>2){
                $sql .= " and (u.user_name like '%".$params->qry_filter."%' ";
                $sql .= " or u.e_mail like '%".$params->qry_filter."%' )";
            }
            if(isset($params->qry_orderCol)){
                $sql .= " order by u.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sql .= "order by u.user_name asc";
            }
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["iii", $params->company_id, $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("users", $ret_json_data);
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_sx.sx_user u
                    where company_id = ? ";
            $bound_params_r = ["i", $params->company_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function UserListTable($params) {
        $mn = "SxCompanyRestHandler::UserListTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id, u.user_name,
                    u.e_mail, u.user_role, u.password as u_password,
                    u.company_id, u.is_receive_emails, 
                    u.ip_address, (UNIX_TIMESTAMP(u.adate)*1000) as adate, (UNIX_TIMESTAMP(u.udate)*1000) as udate,
                    c.company_name, c.branch_code, c.company_type_id, c.country_id
                    FROM iordanov_sx.sx_user u
                    left join iordanov_sm.sm_company c on c.company_id = u.company_id ";
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                $sql .= " WHERE (c.company_name like '%".$params->qry_filter."%' ";
                 $sql .= " or u.user_name like '%".$params->qry_filter."%' ";
                $sql .= " or c.branch_code like '%".$params->qry_filter."%' )";
            }
            
            $sql .= " group by u.user_id ";
            
            if(isset($params->qry_orderCol)){
                $sql .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sql .= "order by user_name, e_mail";
            }
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("user_list", $ret_json_data);
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_sx.sx_user
                    where 1 = ? ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Company Product">
    
    public function CompanyProductsList($company_id) {
        $mn = "SxCompanyRestHandler::CompanyProductsList(".$company_id.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $sql = "SELECT company_product_id,
                    company_id, product_id, adate
                     FROM iordanov_sm.sm_company_product
                     where company_id = ?
                     order by product_id ";
            $bound_params_r = ["i", $company_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("company_products", $ret_json_data);
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyProductAdd($company_id, $product_id) {
        $mn = "SxCompanyRestHandler::CompanyProductAdd(".$company_id.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            
            $sql = "SELECT count(*) as products FROM iordanov_sm.sm_company_product 
                   where company_id = ? and product_id = ? ";
            $bound_params_r = array('ii', $company_id, $product_id);
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            $products = -1;
            if(count($result_r)>0)
            {
                $products = $result_r[0]['products'];
            }
            
            SxLogger::log($mn, " products: " . $products . " ");
            
            if(isset($products) && $products == 0){
                $sql = "INSERT INTO iordanov_sm.sm_company_product
                                ( company_id, product_id) VALUES(?, ?)";
                $bound_params_r = ["ii", $company_id, $product_id];
                $pfid = $conn->preparedInsert($sql, $bound_params_r, $logModel);
                SxLogger::log($mn, " sm_company_product: " . $pfid . " ");
            }
            
            
            $sql = "SELECT company_product_id,
                    company_id, product_id, adate
                     FROM iordanov_sm.sm_company_product
                     where company_id = ?
                     order by product_id ";
            $bound_params_r = ["i", $company_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("company_products", $ret_json_data);
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyProductDelete($company_id, $company_product_id) {
        $mn = "SxCompanyRestHandler::CompanyProductDelete(".$company_id.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            
            $sql = "DELETE FROM iordanov_sm.sm_company_product
                    where company_product_id=?";
            $bound_params_r = ["i", $company_product_id];
            $pfid = $conn->preparedDelete($sql, $bound_params_r, $logModel);
            //SxLogger::log($mn, " sm_company_product: " . $pfid . " ");
            
            $sql = "SELECT company_product_id,
                    company_id, product_id, adate
                     FROM iordanov_sm.sm_company_product
                     where company_id = ?
                     order by product_id ";
            $bound_params_r = ["i", $company_id];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("company_products", $ret_json_data);
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="CompanyItemModel">
    
    public function CompanyItemModelSave($value) {
        $mn = "SxCompanyRestHandler::CompanyItemModelSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj = CompanyItemModel::Save($value);
            $response = new Response("success", "CompanyItemModel data saved.");
            $response->addData("item", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyItemModelImport($value) {
        $mn = "SxCompanyRestHandler::CompanyItemModelImport()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $affected_row_ids = CompanyItemModel::Import($value);
            $response = new Response("success", "CompanyItemModel data imported.");
            $response->addData("itemIds", $affected_row_ids);
            $response->addData("companyId", $value->companyId);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyItemModelGet($value) {
        $mn = "SxCompanyRestHandler::CompanyItemModelGet()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $obj =  CompanyItemModel::LoadById($value);
            $response = new Response("success", "CompanyItemModel get.");
            $response->addData("item", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyItemModelDelete($citemId) {
        $mn = "SxCompanyRestHandler::CompanyItemModelDelete(".$citemId.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = CompanyItemModel::Delete($citemId, $conn, $mn, $logModel);
            
            $response = new Response("success", "CompanyItemModel deleted.");
            $response->addData("citemId", $id);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        // SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
}
