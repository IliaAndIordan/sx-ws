<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: SxProductRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: SxProductRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("Company.class.php");
require_once("UseCase.class.php");

/**
 * Description of SxProductRestHandler
 *
 * @author IZIordanov
 */
class SxProductRestHandler extends SimpleRest {

    // <editor-fold defaultstate="collapsed" desc="Option and Ping">

    public function Option() {
        $mn = "SxProductRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new SxProductRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping() {
        $mn = "SxProductRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Product">
    
    public function ProductSave($dataJson) {
        $mn = "SxProductRestHandler::ProductSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $productId = null;
            if(isset($dataJson->productId)){
               SxLogger::log($mn, "Update productId =" . $dataJson->productId);
               $productId = $dataJson->productId;
               $productId = $this->ProductUpdate($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create product");
                $productId = $this->ProductCreate($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, "productId =" . $productId);
            $ret_json_data = $this->ProductJson($productId, $conn, $mn, $logModel);
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("product", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    function ProductCreate($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_product
            (product_key, product_name, 
            product_image_url, product_note, 
            product_web_url, company_id,
            user_id, adate, udate)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssssiiss",
            ((!isset($dataJson->pkey)) ? null : $dataJson->pkey),
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->note)) ? null : $dataJson->note),
            ((!isset($dataJson->webUrl)) ? null : $dataJson->webUrl),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            (DateTimeDbStr($dataJson->adate) ),
            (DateTimeDbStr($dataJson->udate)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    function ProductUpdate($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_product
            SET
            product_key=?, product_name=?, 
            product_image_url=?, product_note=?,
            product_web_url=?, company_id=?,
            user_id=?
            WHERE product_id = ? " ;

        $bound_params_r = ["sssssiii",
            ((!isset($dataJson->pkey)) ? null : $dataJson->pkey),
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->note)) ? null : $dataJson->note),
            ((!isset($dataJson->webUrl)) ? null : $dataJson->webUrl),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ($dataJson->productId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->productId;
    }
    
    function ProductJson($productId, $conn, $mn, $logModel){
        
        $sql = "SELECT p.product_id as productId, p.product_key as pkey,
                    p.product_name as name, p.product_image_url as imageUrl, 
                    p.product_note as note, p.product_web_url as webUrl, 
                    p.company_id as companyId, c.company_name as companyName, c.branch_code as branch,
                    p.user_id  as userId, u.user_name as userName, u.e_mail as email,
                     p.adate, p.udate
                    FROM iordanov_psm.psm_product p
                    left join iordanov_sx.sx_user u on u.user_id = p.user_id 
                    left join iordanov_sm.sm_company c on c.company_id = p.company_id
                    WHERE p.product_id = ? " ;

        $bound_params_r = ["i",$productId];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Product Table">
    
    public function ProductTable($params) {
        $mn = "SxProductRestHandler::ProductTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT p.product_id as productId, p.product_key as pkey,
                    p.product_name as name, p.product_image_url as imageUrl, 
                    p.product_note as note, p.product_web_url as webUrl, 
                    p.company_id as companyId, c.company_name as companyName, c.branch_code as branch,
                    p.user_id  as userId, u.user_name as userName, u.e_mail as email,
                     p.adate, p.udate
                    FROM iordanov_psm.psm_product p
                    left join iordanov_sx.sx_user u on u.user_id = p.user_id 
                    left join iordanov_sm.sm_company c on c.company_id = p.company_id ";
            
            $sqlWhere = null;
            $sqlOrder = null;
            if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE p.company_id = ".$params->companyId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or p.product_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or p.product_name like '%".$params->qry_filter."%' )";
                }
               
            }
            
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by p.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by p.udate desc, p.product_name asc";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("product_list", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_psm.psm_product p ".$sqlWhere." and 1=?"  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
   
    // </editor-fold>
    
}
