<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $_sxLogger;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__) . '/appender_pdo.properties');
/**
 * Description of Log
 *
 * @author izior
 */
class SxLogger extends LoggerBase
{
    
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct()
    {
        parent::__construct();
        $this->MN = "SxLogger: ";
        try {


            $this->logger = Logger::getRootLogger();

            $this->logger->debug("SxLogger init");

        } catch (Exception $ex) {
            echo "SxLogger Error: " . $ex . "<br/>";
        }
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function loggerWad()
    {
        global $_sxLogger;
        if (!isset($_sxLogger)) {
            $_sxLogger = new SxLogger();
        }

        return $_sxLogger;
    }

    public static function currLogger()
    {
        global $_sxLogger;
        if (!isset($_sxLogger)) {
            $_sxLogger = new SxLogger();
        }

        return $_sxLogger;
    }

    public static function logBegin($mn)
    {
        SxLogger::currLogger()->begin($mn);
    }

    public static function logEnd($mn)
    {
        SxLogger::currLogger()->end($mn);
    }

    public static function log($mn, $msg)
    {
        SxLogger::currLogger()->debug($mn, $msg);
    }

    public static function logError($mn, $ex)
    {
        SxLogger::currLogger()->error($mn);
    }
    // </editor-fold>
}
