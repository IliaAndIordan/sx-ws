<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: SxRestHandler.class.php
 *
 * Database System        	: MySQL
 * Created from                 : IordIord
 * Date Creation		: 19.12.2018
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: SxRestHandler.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("SimpleRest.class.php");
require_once("Response.class.php");
require_once("SxConnection.php");
require_once("SxLogger.php");
require_once("JwtAuth.php");
require_once("SxUser.class.php");
require_once("UserModel.class.php");
require_once("Company.class.php");

/**
 * Description of SxRestHandler
 *
 * @author IZIordanov
 */
class SxRestHandler extends SimpleRest
{
    
    // <editor-fold defaultstate="collapsed" desc="Option, Ping">

    public function Option()
    {
        $mn = "SxRestHandler::Option()";
        $response = new Response("success", "Service working.");

        $rh = new SxRestHandler();
        $rh->EncodeResponce($response);
    }

    public function Ping()
    {
        $mn = "SxRestHandler::Ping()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            if (isset($conn)) {
                SxLogger::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            } else {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }

        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="JWT Tocken Methods">
    
    public function Login($email, $password)
    {
        $mn = "SxRestHandler::Login('.$email.', '.$password.')";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $user = SxUser::login($email, $password);
            if (isset($user)) {
                SxLogger::log($mn, " user = " . $user->toJson());
                if (JwtAuth::signTocken($user->getId(), $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("current_user", $user);
                    SxLogger::log($mn, " company_id = " . $user->company_id);
                    if(isset($user) && isset($user->company_id)){
                        $com = new Company();
                        $com->LoadById($user->company_id);
                        $response->addData("company", $com);  
                        
                        $sql = "SELECT company_product_id,
                            company_id, product_id, adate
                             FROM iordanov_sm.sm_company_product
                             where company_id = ?
                             order by product_id ";
                        $conn = SxConnection::dbConnect();
                        $logModel = SxLogger::currLogger()->getModule($mn);
                        $bound_params_r = ["i", $user->company_id];
                        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                        $response->addData("products", $ret_json_data);
                    }
                    
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentials.");
                $response->statusCode = 401;
            }

        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    

    public function refreshToken($refresh)
    {
        $mn = "SxRestHandler::refreshToken()";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $user = JwtAuth::RefreshTocken($refresh);
             if (isset($user)) {
                SxLogger::log($mn, " user = " . json_encode($user));
                if (JwtAuth::signTocken($user->userId, $user)) {
                    $response = new Response("success", "JwtAuth Set.");
                    $response->addData("current_user", $user);
                    SxLogger::log($mn, " company_id = " . $user->companyId);
                    if(isset($user) && isset($user->companyId)){
                        $com = new Company();
                        $com->LoadById($user->companyId);
                        $response->addData("company", $com);  
                    }
                    
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 412;
                }
            } else {
                $response = new Response("error", "Invalid Refresh Token.");
                $response->statusCode = 412;
            }
            

        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        //SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="SxUser">
    
    public function Register($eMail, $password)
    {
        $mn = "SxRestHandler::register('.$eMail.', '.$password.)";
        SxLogger::logBegin($mn);
        $response = null;
        
        try {
            $wrongEmail = false;
            if(!isset($eMail) || !isset($password)){
                $wrongEmail = true;
            }
             
            if(!$wrongEmail ){
                $wrongEmail = !checkEmail($eMail);
                SxLogger::log($mn, "checkEmail = " . $wrongEmail?'true':'false');
            }
            
            if(!$wrongEmail){
                $wrongEmail=SxUser::isExistEMail($eMail);
                if($wrongEmail){
                     $response = new Response("error", "E-Mail already registered.");
                     $response->statusCode = 200;
                }
            }
            
            
            if (!$wrongEmail) {
                $user = SxUser::CreateRowData($eMail, $password);
                
                SxLogger::log($mn, "new user = " . $user->toJSON());
                 if (JwtAuth::signTocken($user->getId(), $user->getName())) {
                    $response = new Response("success", "JwtAuth Set.");
                    $currSxUser = new SxUserModel($user);

                    $response->addData("current_user", $currSxUser);
                } else {
                    $response = new Response("error", "JwtAuth NOT Set.");
                    $response->statusCode = 401;
                }
            } else {
                $response = new Response("error", "Invalid Credentioals.");
                $response->statusCode = 200;
            }

        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function SxUserSave($value) {
        $mn = "SxRestHandler::SxUserSave()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $obj = new SxUser();
            $obj->setId($value->user_id);
            $obj->setName($value->user_name);
            $obj->setEMail($value->e_mail);
            $obj->setPassword($value->u_password);
            $obj->setCompanyId($value->company_id);
            $obj->setRole($value->user_role);
            $obj->setIsReceiveEMails($value->is_receive_emails);

            $ret_json_data = $obj->save();
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("user", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    public function CompanyList($userId) {
        $mn = "SxRestHandler::CompanyList(".$userId.")";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT SQL_CACHE user_id, e_mail, 
                is_receive_emails, user_name
                FROM iordanov_bwt.bwt_user
                where user_id=?";
            $bound_params_r = ["i", $userId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("book_autor",$ret_json_data);
            
        } catch (Exception $ex) {
             SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        
        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
}
